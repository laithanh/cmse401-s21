#!/bin/bash

echo "" > time.txt 
for n in {1..10};
do
	echo $n
	{ time $1 ; } 2>> time.txt
done
avtime=`cat time.txt | grep real | cut -d "m" -f 2 | cut -d "s" -f1 | jq -s add/length`

echo ""
echo ""
echo "The average time after 10 runs is $avtime"

